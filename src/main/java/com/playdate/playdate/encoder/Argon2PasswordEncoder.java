package com.playdate.playdate.encoder;

import org.springframework.security.crypto.password.PasswordEncoder;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

public class Argon2PasswordEncoder implements PasswordEncoder {

    private static final Argon2 ARGON2 = Argon2Factory.create();

    private static final int ITERATIONS = 2;
    private static final int MEMORY= 65536;
    private static final int PARALLELISM = 1;

    /**
     * Gives back an encoded password that is random every single time when a new user is created
     * @param rawPassword
     * @return An encoded password that is random every single time when a new user is created
     */

    public String encode(final CharSequence rawPassword) {
        //hash returns already the encoded String
        final String hash = ARGON2.hash(ITERATIONS, MEMORY, PARALLELISM, rawPassword.toString());
        return hash;
    }


    /**
     * Gives back true
     * if the password written in the field when it is in its raw non-encoded password is equal to the encoded passowrd
     * else false
     * @param rawPassword the raw non-encoded password
     * @param encodedPassword the encoded password with Argon2
     * @return true
     * if the password written in the field when it is in its raw non-encoded password is equal to the encoded passowrd
     * else false
     */
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return ARGON2.verify(encodedPassword, rawPassword.toString());
    }

}
