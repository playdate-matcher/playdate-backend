package com.playdate.playdate;

import com.playdate.playdate.models.AuthenticationResponse;
import com.playdate.playdate.models.Parent;
import com.playdate.playdate.services.MyUserDetailsService;
import com.playdate.playdate.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
public class Authorization {

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    /**
     *
     * @param parent the parent object that we use when we input our username and password
     * @return a JWT token that we can use to login with
     */
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody Parent parent) {
            UserDetails userDetails = null;
            if(!(parent.getEmail().equals("") || parent.getPassword().equals("")))
                userDetails = myUserDetailsService.loadUser(parent.getEmail(), parent.getPassword());

            if (userDetails == null){
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Fel användarnamn eller lösenord");
            }
            final String jwt = jwtTokenUtil.generateToken(userDetails);

            return ResponseEntity.status(HttpStatus.OK).body(new AuthenticationResponse(jwt));
    }
}