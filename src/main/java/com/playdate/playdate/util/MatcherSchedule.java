package com.playdate.playdate.util;
import com.playdate.playdate.models.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import java.sql.Timestamp;
import java.util.*;
import com.playdate.playdate.firebase.*;

@Component
public class MatcherSchedule {

    @Autowired
    PlayDateRepository playDateRepository;
    @Autowired
    FriendsWithRepository friendsWithRepository;
    @Autowired
    NotificationRepository notificationRepository;
    @Autowired
    MatchRepository matchRepository;
    @Autowired
    ParentRepository parentRepository;
    @Autowired
    PushNotificationService pushNotificationService;


    /**
     * Periodisk, kör klockan 1 på natten, tar bort alla frysningar på vänskaper.

    @Scheduled(cron = "0 0 1 * * ?")
    public void unfreezer(){
        List<FriendsWith> friendships = friendsWithRepository.findAll();
        for(FriendsWith fw : friendships){
            if(fw.isFrozen()){
                fw.setFrozen(false);
                friendsWithRepository.save(fw);
            }
        }
    }
    */
    /**
     * Periodisk, skickar push-notiser för PlayDates var framförhållning passerat
     */
    @Scheduled(fixedDelay = 1000*60*5)
    public void searchAlert(){
        List<PlayDate> notifiable = playDateRepository.getForForesightPassed();
        timeFilterForNotification(notifiable);
        for(PlayDate pd : notifiable){
            notificationRepository.createNotification(new Timestamp(System.currentTimeMillis()), pd.getChildParentEmail(), pd.getId(), null);
            //push-notis
            String title = "Deadline passerad!";
            String message = "Lekstunden för " + pd.getChildFirstName() + " kommer inte matchas.";
            pushNotification(pd, title, message);
        }

    }

    /**
     * Filters the provided list after the time limits:
     * - removes all PlayDates that start in more than 40 min (tooLate)
     * @param list
    */
    private void timeFilterForNotification(List<PlayDate> list){
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Timestamp earlier = new Timestamp(System.currentTimeMillis() - (long)1000*60*5);
        list.removeIf(pd -> !(pd.getForesight().before(now) && pd.getForesight().after(earlier)));
    }

    private void pushNotification(PlayDate pd, String title, String message){
        Parent p = parentRepository.findById(pd.getChildParentEmail()).orElse(null);
        String token = p.getFcmToken();
        PushNotificationRequest push = new PushNotificationRequest(title, token, message);
        pushNotificationService.sendPushNotificationToToken(push);
    }

    /**
     * Periodisk, matchar ihop två PlayDates som överlappar i tid och villkor.
     * Prioriterar längsta lektid, om lika långa, prioriterar den som kan börja snarast.
     * Skickar push-notiser till berörda parter.
     */

    /**
     * Goes through the database to find all matches. After it has been completed it waits for 1 min before running again.
     */
    @Scheduled(fixedDelay = 1000*60*5)
    public void matcher(){
        List<PlayDate> allMatchable = playDateRepository.getMatchablePlayDates();
        timeFilterForMatch(allMatchable);
        Map<PlayDate, ArrayList<PlayDate>> matchesPerPD = new HashMap<PlayDate, ArrayList<PlayDate>>(allMatchable.size()/4);
        playDatePossibilities(allMatchable, matchesPerPD);//every PlayDate get a list with possible matches
        if(matchesPerPD.isEmpty()){// no PlayDates to match and therefore completed the task
            return;
        }

        Set<Map.Entry<PlayDate, PlayDate>> toMatch = getMatches(matchesPerPD);//gives the PlayDates that can match

        for(Map.Entry<PlayDate, PlayDate> me : toMatch){
            PlayDate pd1 = me.getKey();
            PlayDate pd2 = me.getValue();
            Timestamp start = pd1.getStartTime().after(pd2.getStartTime()) ? pd1.getStartTime() : pd2.getStartTime();
            Timestamp end = pd1.getEndTime().before(pd2.getEndTime()) ? pd1.getEndTime() : pd2.getEndTime();
            matchRepository.createMatch(start.toString(),end.toString(), pd1.getId(), pd2.getId());
            Match m = matchRepository.findByPlayDateId1AndPlayDateId2(pd1.getId(), pd2.getId());
            pd1.setMatchId(m.getId());
            pd2.setMatchId(m.getId());
            playDateRepository.save(pd1);
            playDateRepository.save(pd2);
            notificationRepository.createNotification(new Timestamp(System.currentTimeMillis()), pd1.getChildParentEmail(), pd1.getId(), m.getId());
            notificationRepository.createNotification(new Timestamp(System.currentTimeMillis()), pd2.getChildParentEmail(), pd2.getId(), m.getId());
            //pushnotiser
            String title = "Matchning!";
            String message1 = pd1.getChildFirstName() + " kan leka med " + pd2.getChildFirstName();
            String message2 = pd2.getChildFirstName() + " kan leka med " + pd1.getChildFirstName();
            pushNotification(pd1, title, message1);
            pushNotification(pd2, title, message2);
        }

    }

    /**
     * filters the provided list with the right times for a match:
     * - removes all PlayDates where foresight has passed
     * - removes all PlayDates that ends within an hour
     * @param pdList
     */
    private void timeFilterForMatch(List<PlayDate> pdList){
        Timestamp tooLate = new Timestamp(System.currentTimeMillis() + (long)1000*60*59);
        pdList.removeIf(pd -> pd.getEndTime().before(tooLate));
    }


    /**
     * Fills the provided map with where the key of a PlayDate and its value is a list of the PlayDates it can Match with
     * @param matchables The PlayDates that can me matched.
     * @param map to be filled with the possible matches.
     */
    private void playDatePossibilities(List<PlayDate> matchables, Map<PlayDate, ArrayList<PlayDate>> map){
        for(PlayDate pd : matchables){
            List<FriendsWith> friends = friendsWithRepository.findByChildFirstName1AndChildParentEmail1(pd.getChildFirstName(), pd.getChildParentEmail());
            ArrayList<PlayDate> possibleMatches = resolveFriendshipMatch(pd, friends, matchables);
            possibleMatches.removeIf(p -> (pd.getStartTime().after(p.getEndTime()) || pd.getEndTime().before(p.getStartTime())));//Tar bort lekstunder som som inte överlappar
            if(!possibleMatches.isEmpty()){
                map.put(pd, possibleMatches);
            }
        }
    }

    /**
     * Gives a list with PlayDates where the child is friendsWith and the friend is not frozen/paused.
     * @param source The playdate to find matches
     * @param friends List of the child's friends
     * @param pds List of PlayDates
     * @return ArrayList containing matchable playdates according to friendships
     */
    private ArrayList<PlayDate> resolveFriendshipMatch(PlayDate source, List<FriendsWith> friends, List<PlayDate> pds){
        ArrayList<PlayDate> res = new ArrayList<PlayDate>();
        ChildId child = new ChildId(source.getChildFirstName(), source.getChildParentEmail());
        for(PlayDate pd : pds){
            ChildId player = new ChildId(pd.getChildFirstName(), pd.getChildParentEmail());
            if(!child.equals(player)) { // ignores the PlayDates own child
                for (FriendsWith friendsWith : friends) {
                    ChildId friend = new ChildId(friendsWith.getChildFirstName2(), friendsWith.getChildParentEmail2());
                    if(player.equals(friend) && !friendsWith.isFrozen()){
                        FriendsWith reverse = friendsWithRepository.findByChildFirstName1AndChildParentEmail1AndChildFirstName2AndChildParentEmail2(
                                friendsWith.getChildFirstName2(), friendsWith.getChildParentEmail2(), source.getChildFirstName(), source.getChildParentEmail());
                        if(reverse != null) {
                            if (!reverse.isFrozen()) {
                                res.add(pd);
                            }
                        }
                    }

                }
            }
        }
        return res;
    }

    /**
     * Creates a set containing two PlayDates that are to be matched.
     * @param listMap
     * @return
     */
    private Set<Map.Entry<PlayDate, PlayDate>> getMatches(Map<PlayDate, ArrayList<PlayDate>> listMap){
        Map<PlayDate, PlayDate> matchMap = new HashMap<>();
        while(longestPlayTime(matchMap, listMap)){
            // runs longestPlayTime  till it can't anymore
        }
        return matchMap.entrySet();

    }

    /**
     * Puts the PlayDates in order after their matched duration time: longest playdate match first and the shortest last.
     * @param matchMap
     * @param listMap
     * @return
     */
    private boolean longestPlayTime(Map<PlayDate, PlayDate> matchMap, Map<PlayDate, ArrayList<PlayDate>> listMap){
        if(listMap.isEmpty()){
            return false;
        }
        PlayDate pd1 = null, pd2 = null;
        long longest = 0;
        Timestamp start;
        Timestamp end;
        Timestamp earliest = new Timestamp(0);
        for (Map.Entry<PlayDate, ArrayList<PlayDate>> pair : listMap.entrySet()) {
            PlayDate pd = pair.getKey();
            ArrayList<PlayDate> list = pair.getValue();
            Timestamp keyStart = pd.getStartTime();
            Timestamp keyEnd = pd.getEndTime();
            for(PlayDate p : list){
                Timestamp valueStart = p.getStartTime();
                Timestamp valueEnd = p.getEndTime();
                if(keyStart.after(valueStart)){
                    start = keyStart;
                }else{
                    start = valueStart;
                }
                if(keyEnd.before(valueEnd)){
                    end = keyEnd;
                }else{
                    end = valueEnd;
                }
                long diff = end.getTime() - start.getTime();
                if(matchConditions(pd, p, diff)) { //checks booleans and time
                    if (diff == longest && start.before(earliest)) {//prioritizes the one starting earliest if they have the same duration
                        earliest = start;
                        pd1 = pd;
                        pd2 = p;
                    }else if(diff > longest){
                        earliest = start;
                        longest = diff;
                        pd1 = pd;
                        pd2 = p;
                    }
                }
            }
        }
        if(pd1 == null || pd2 == null){
            return false;
        }
        matchMap.put(pd1, pd2);
        listMap.remove(pd1);
        listMap.remove(pd2);
        for(Map.Entry<PlayDate, ArrayList<PlayDate>> me: listMap.entrySet()){
            ArrayList<PlayDate> pdl = me.getValue();
            PlayDate pd11 = pd1;
            PlayDate pd22 = pd2;
            pdl.removeIf(p -> (p.equals(pd11)));
            pdl.removeIf(p -> (p.equals(pd22)));
        }
        return true;
    }

    private boolean matchConditions(PlayDate pd1, PlayDate pd2, long time){
        boolean canGet1 = pd1.isCanGet();
        boolean canGet2 = pd2.isCanGet();
        boolean canDrop1 = pd1.isCanDrop();
        boolean canDrop2 = pd2.isCanDrop();
        boolean canHost1 = pd1.isCanHost();
        boolean canHost2 = pd2.isCanHost();
        return(((canGet1 || canGet2) && (canDrop1 || canDrop2) && (canHost1 || canHost2)) && time > (long)1000*60*60);
    }
}