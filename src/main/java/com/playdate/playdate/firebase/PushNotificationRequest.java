package com.playdate.playdate.firebase;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PushNotificationRequest {

    private String title;
    private String message;
    private String token;
    private String topic;
    public PushNotificationRequest() {
    }

    public PushNotificationRequest(String title, String token, String messageBody) {
        this.title = title;
        this.token = token;
        this.message = messageBody;
    }
}