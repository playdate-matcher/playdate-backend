package com.playdate.playdate.services;

import com.playdate.playdate.encoder.Argon2PasswordEncoder;
import com.playdate.playdate.models.Parent;
import com.playdate.playdate.models.ParentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class MyUserDetailsService implements UserDetailsService {


    @Autowired
    ParentRepository parentRepository;

    /**
     * Retrieves the UserDetails from the database of the provided email.
     * @param email
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Parent parent =  parentRepository.findById(email).orElse(null);
        if (parent.getEmail().equals(email)) {
            String password = parent.getPassword();
            return new User(email, password, new ArrayList<>());
        }
        return null;
    }

    /**
     * Checks if a user with the provided email and password match the databases encoded password and email.
     * @param email
     * @param password
     * @return UserDetail if user was found and null if not.
     */
    public UserDetails loadUser(String email, String password) {
        Argon2PasswordEncoder encoder = new Argon2PasswordEncoder();
        Parent parent = parentRepository.findById(email).orElse(null);
        if (parent != null && encoder.matches(password, parent.getPassword()) && parent.getEmail().equals(email)) {
            return new User(email, password, new ArrayList<>());
        }
        return null;
    }
}
