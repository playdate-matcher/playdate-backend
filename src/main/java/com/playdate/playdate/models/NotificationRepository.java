package com.playdate.playdate.models;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, String> {

    /**
     * Removes all Notifications from the database where the given parentEmail matches.
     * @param parentEmail
     */
    @Query(value = "DELETE FROM Notifications where parentEmail = :parentEmail", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteParentsNotificationsWithEmail(
            @Param("parentEmail") String parentEmail
    );

    /**
     * Sets all MatchIds and PlayDateIds to NULL where the given parentEmail matches.
      * @param parentEmail
     */
    @Query(value = "UPDATE Notifications SET matchId = NULL, playDateId = NULL WHERE parentEmail = :parentEmail", nativeQuery = true)
    @Modifying
    @Transactional
    void setMatchIdAndPlayDateIdToNull(
            @Param("parentEmail") String parentEmail
    );

    /**
     * Creates and saves a notification with the information given. Either playDateId or matchId will be null.
     * @param created time creates
     * @param parentEmail
     * @param playDateId
     * @param matchId
     */
    @Query(value = "insert into Notifications values (null, :created,  :parentEmail, :playDateId, :matchId); ", nativeQuery = true)
    @Modifying
    @Transactional
    void createNotification(
                    @Param("created") Timestamp created,
                    @Param("parentEmail") String parentEmail,
                    @Param("playDateId") Integer playDateId,
                    @Param("matchId") Integer matchId
    );

    /**
     * Retrieves a list of all the notifications with the parent email given.
     * @param parentEmail
     * @return list of Notifications
     */
    List<Notification> findByParentEmail(String parentEmail);

    /**
     * Retrieves a list of all match-notifications with the parent email given.
     * @param parentEmail
     * @return list of Notifications
     */
    List<Notification> findByParentEmailAndMatchIdNotNull(String parentEmail);
    /**
     * Retrieves a Notification with the given playDateId.
     * @param playDateId
     * @return Notification
     */
    Notification findByPlayDateId(int playDateId);
}