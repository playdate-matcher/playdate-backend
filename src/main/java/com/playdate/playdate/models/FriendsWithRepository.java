package com.playdate.playdate.models;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

import java.util.List;

@Repository
public interface FriendsWithRepository extends JpaRepository<FriendsWith, String> {

    /**
     * Creates a list of all friendsWith a children has.
     * @param name1Input the child's first name
     * @param pEmail1Input the parents email
     * @return list of FriendsWith objects
     */
    List<FriendsWith> findByChildFirstName1AndChildParentEmail1(String name1Input, String pEmail1Input);

    /**
     * Finds a FriendsWith between two children.
     * @param name1Input first child's first name
     * @param pEmail1Input first child's parents email
     * @param n2 second child's first name
     * @param p2 second child's parents email
     * @return FriendsWith between the two children
     */
    FriendsWith findByChildFirstName1AndChildParentEmail1AndChildFirstName2AndChildParentEmail2(
            String name1Input, String pEmail1Input, String n2, String p2);

    /**
     * Deletes all friendsWith connected to the email given.
     * @param parentEmail
     */
    @Query(value = "DELETE FROM FriendsWith where childParentEmail1 = :parentEmail OR childParentEmail2 = :parentEmail", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteFriendsWithWithEmail(
            @Param("parentEmail") String parentEmail
    );

    /**
     * Updates the friendsWith to be either frozen or not frozen.
     * @param freeze the value to give frozen
     * @param firstName first name of the current users child
     * @param friendsName first name of the friend
     * @param parentsEmail email to the current users parent
     * @param friendsParentsEmail email to the friends parent
     */
    @Query(value = "UPDATE FriendsWith SET frozen = :freeze " +
            "where childFirstName1 = :firstName AND childFirstName2 = :friendsName" +
            " AND childParentEmail1 = :parentsEmail AND childParentEmail2 = :friendsParentsEmail" , nativeQuery = true)
    @Modifying
    @Transactional
    void freezeFriend(
            @Param("freeze") Boolean freeze,
            @Param("firstName") String firstName,
            @Param("friendsName") String friendsName,
            @Param("parentsEmail") String parentsEmail,
            @Param("friendsParentsEmail") String friendsParentsEmail);
}