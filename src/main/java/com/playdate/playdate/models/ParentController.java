package com.playdate.playdate.models;

import com.playdate.playdate.encoder.Argon2PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.sound.midi.SysexMessage;
import java.util.List;
import java.util.Map;

@RestController
public class ParentController {

    @Autowired
    ParentRepository parentRepository;
    @Autowired
    NotificationRepository notificationRepository;
    @Autowired
    PlayDateRepository playDateRepository;
    @Autowired
    ChildRepository childRepository;
    @Autowired
    MatchRepository matchRepository;
    @Autowired
    FriendsWithRepository friendsWithRepository;

    /**
     * Creates a new parent with the given parameters.
     * @param parent has to contain: firstName, email and password.
     *               Optional fields are:  address, phoneNbr and surname.
     *               Example:
     *               {
     * 	                "firstName": "Klara",
     * 	                "email": "klara@hotmail.com",
     * 	                "password": "wkjnejeee"
     * 	                "address": "Bond street 20"
     *              }
     * @return ResponseEntity<Object> saying "user created" or "Email is already in use"
     */
    @ResponseBody
    @PostMapping("/register")
    public ResponseEntity<Object> createUser(@RequestBody Parent parent){
        Argon2PasswordEncoder encoder = new Argon2PasswordEncoder();
        List<Parent> all = parentRepository.findAll();

        for (Parent oldParent : all) {
            if (oldParent.getEmail().equals(parent.getEmail())) {

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email används redan");
            }
        }
        parent.setPassword(encoder.encode(parent.getPassword()));
        parentRepository.save(parent);
        return ResponseEntity.status(HttpStatus.CREATED).body("Användare skapad");
    }

    /**
     * Retrieves the current users children.
     * @return list of all Child objects belonging to the current user.
     */
    @GetMapping("/child")
    public List<Child> getChildren(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return childRepository.findByParentEmailContaining(auth.getName());
    }

    /**
     * Removes the current parent and all of its data from the database.
     * @return ResponseEntity<Object> saying "Användare borttagen" if completed.
     */
    @ResponseBody
    @DeleteMapping("/parent")
    public ResponseEntity<Object> removeCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();
        notificationRepository.setMatchIdAndPlayDateIdToNull(email);
        notificationRepository.deleteParentsNotificationsWithEmail(email);
        List<PlayDate> playDateList = playDateRepository.findAllParentsPlayDates(email);

        for(PlayDate p: playDateList){
            if (p.getMatchId() != null) {
                Match match = matchRepository.findMatchWithPlayDateId(p.getId());
                String friendsEmail = parentRepository.getParentEmail((p.getId() == match.getPlayDateId1() ? match.getPlayDateId2() : match.getPlayDateId1()));
                notificationRepository.setMatchIdAndPlayDateIdToNull(friendsEmail);
                notificationRepository.deleteParentsNotificationsWithEmail(friendsEmail);
                playDateRepository.setMatchIdToNull(match.getPlayDateId2());
                playDateRepository.setMatchIdToNull(match.getPlayDateId1());
                matchRepository.deleteMatchWithMatchId(p.getMatchId());
            }
        }
        playDateRepository.deleteParentsPlayDates(email);
        friendsWithRepository.deleteFriendsWithWithEmail(email);
        childRepository.deleteParentsChildren(email);
        parentRepository.deleteParent(email);
        return ResponseEntity.status(HttpStatus.OK).body("Användare borttagen");
    }

    /***
     *
     * @param updates map where containing none or many of the keys: surname, address, phoneNbr and fcmToken.
     *                The key's value is the updated value. Example:
     *                {
     *                  "surname": "Bergman",
     * 	                "phoneNbr": "0743784647",
     * 	                "fcmToken": "nkjwfeuhfklern"
     *                }
     * @return ResponseEntity<Object> saying "user updated if completed"
     */
    @PatchMapping("/parent")
    public ResponseEntity<Object> updateCurrentUser(@RequestBody Map<String, Object> updates){
        Parent parent = parentRepository.getParent(SecurityContextHolder.getContext().getAuthentication().getName());
        if (updates.size() != 0) {
            parentRepository.updateParent(parent.getEmail(),
                    (String) updates.getOrDefault("surname", parent.getSurname()),
                    (String) updates.getOrDefault("address", parent.getAddress()),
                    (String) updates.getOrDefault("phoneNbr", parent.getPhoneNbr()),
                    (String) updates.getOrDefault("fcmToken", parent.getFcmToken()));
            return ResponseEntity.status(HttpStatus.OK).body("Dina uppgifter är updaterade");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Inga ifyllda uppgifter");
    }
}