package com.playdate.playdate.models;

import lombok.Getter;

/**
 * Class is used when someone wants to delete a friend.
 */
@Getter
public class DeleteRequest {
    private String firstName;
    private String friendsName;
    private String friendsParentEmail;

}
