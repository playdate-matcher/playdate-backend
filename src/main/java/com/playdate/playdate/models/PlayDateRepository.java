package com.playdate.playdate.models;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PlayDateRepository extends JpaRepository<PlayDate, String> {


    /**
     * Retrieves a list of all the PlayDates belonging to the provided child's name and parents email.
     * @param childParentEmail
     * @param childFirstName
     * @return list of PlayDate
     */
    List<PlayDate> findByChildParentEmailAndChildFirstName(String childParentEmail, String childFirstName);


    /**
     * Retrieves all the PlayDates belonging to the provided parents email
     * @param parentEmail
     * @return list of PlayDate
     */
    @Query(value = "select * from PlayDates where childParentEmail = :parentEmail", nativeQuery = true)
    List<PlayDate> findAllParentsPlayDates(
            @Param("parentEmail") String parentEmail
    );

    /**
     * Updates the matchId to null where the provided playDateId is.
     * @param playDateId
     */
    @Query(value = "UPDATE PlayDates SET matchId = NULL where id = :playDateId", nativeQuery = true)
    @Modifying
    @Transactional
    void setMatchIdToNull(
            @Param("playDateId") int playDateId
    );

    /**
     * Deletes all PlayDates belonging to the email provided.
     * @param parentEmail
     */
    @Query(value = "DELETE FROM PlayDates where childParentEmail = :parentEmail", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteParentsPlayDates(
            @Param("parentEmail") String parentEmail
    );

    /**
     *  Retrieves all PlayDates that has not had a match and end time has not expired.
     * @return list of PlayDates that can be matched.
     */
    @Query(value = "select * from PlayDates WHERE matchID is NULL AND endTime > CURRENT_TIMESTAMP AND foresight > CURRENT_TIMESTAMP", nativeQuery = true)
    List<PlayDate> getMatchablePlayDates();

    /**
     *  Retrieves all PlayDates that has not had a match and end time has not expired.
     * @return list of PlayDates that can be matched.
     */
    @Query(value = "select * from PlayDates WHERE matchID is NULL AND endTime > CURRENT_TIMESTAMP AND foresight < CURRENT_TIMESTAMP", nativeQuery = true)
    List<PlayDate> getForForesightPassed();


    /**
     * Retrieves the playDate with the provided playDateId.
     * @param playDateId
     * @return PlayDate
     */
    @Query(value = "select * from PlayDates WHERE ID = :playDateId", nativeQuery = true)
    PlayDate getPlayDateId(
            @Param("playDateId") int playDateId
    );


    /**
     * Finds all PlayDates (two or empty) where the matchId is the matchId provided
     * @param matchId
     * @return list of PlayDate
     */
    @Query(value = "select * from PlayDates where matchID = :matchId", nativeQuery = true)
    List<PlayDate> getPlayDatesWithMatchId(
            @Param("matchId") int matchId
    );
}