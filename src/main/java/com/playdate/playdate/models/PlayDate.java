package com.playdate.playdate.models;

import javax.persistence.*;

import lombok.Setter;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "PlayDates")
public class PlayDate {

    @Id
    private int id;
    private Timestamp startTime;
    private Timestamp endTime;
    private Timestamp foresight;
    private boolean canGet;
    private boolean canDrop;
    private boolean canHost;
    private String childFirstName;
    private String childParentEmail;
    private Integer matchId;

    /**
     * Creates a PlayDate with no parameters.
     * WARNING: Is used although says it is not.
     */
    public PlayDate() {}

    /**
     * Uses the id as hashcode instead of the provided hashcode.
     * @return id
     */
    @Override
    public int hashCode() {
        return id;
    }

    /**
     * Compares the id's of this object and the parameter object
     * @param o
     * @return true if the ids match
     */
    @Override
    public boolean equals(Object o){
        if(o instanceof PlayDate){
            PlayDate p = (PlayDate) o;
            return (p.getId() == id);
        }
        return false;
    }
}