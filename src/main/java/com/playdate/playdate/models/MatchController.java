package com.playdate.playdate.models;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class MatchController {

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    PlayDateRepository playDateRepository;

    @Autowired
    ChildRepository childRepository;

    @Autowired
    ParentRepository parentRepository;
    @Autowired
    NotificationRepository notificationRepository;



    @ResponseBody
   // @GetMapping("/match")
    public MatchInfo getMatch(@RequestParam int matchId) throws NoSuchElementException{
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MatchInfo matchInfo = new MatchInfo();
        matchInfo.setMatchId(matchId);
        Match match = matchRepository.findMatchWithMatchId(matchId);
        if (match == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Ingen matchning hittades");
        }
        //all info from Match rep
        matchInfo.setStartTime(match.getStartTime());
        matchInfo.setEndTime(match.getEndTime());

        //all info from playdates rep
        List<PlayDate> playDates = playDateRepository.getPlayDatesWithMatchId(matchId);
        PlayDate currentUsersPlayDate = playDates.get(0).getChildParentEmail().contains(auth.getName()) ? playDates.get(0) : playDates.get(1);
        PlayDate matchedPlayDate = playDates.get(0).getChildParentEmail().contains(auth.getName()) ? playDates.get(1) : playDates.get(0);
        matchInfo.setCanDrop(booleanConverterForMatch(currentUsersPlayDate.isCanDrop(), matchedPlayDate.isCanDrop()));
        matchInfo.setCanGet(booleanConverterForMatch(currentUsersPlayDate.isCanGet(), matchedPlayDate.isCanGet()));
        matchInfo.setCanHost(booleanConverterForMatch(currentUsersPlayDate.isCanHost(), matchedPlayDate.isCanHost()));

        //all from children rep
        Child currentUsersChild = childRepository.findChild(currentUsersPlayDate.getChildFirstName(), currentUsersPlayDate.getChildParentEmail());
        matchInfo.setMyChildFullName(currentUsersChild.getFirstName() + " " + currentUsersChild.getSurname());
        Child matchedChild = childRepository.findChild(matchedPlayDate.getChildFirstName(), matchedPlayDate.getChildParentEmail());
        matchInfo.setMatchedChildFullName(matchedChild.getFirstName() + " " + matchedChild.getSurname());
        matchInfo.setMatchedChildAllergies(matchedChild.getAllergies());

        //all from parents rep
        Parent matchedParent = parentRepository.getParent(matchedPlayDate.getChildParentEmail());
        matchInfo.setMatchedParentFullName(matchedParent.getFirstName() + " " + matchedParent.getSurname());
        matchInfo.setMatchedParentAddress(matchedParent.getAddress());
        matchInfo.setMatchedParentNumber(matchedParent.getPhoneNbr());
        return matchInfo;
    }
    @ResponseBody
    @GetMapping("/match")
    public ResponseEntity<Object> getMatches(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String parent = auth.getName();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        List<Notification> nList = notificationRepository.findByParentEmailAndMatchIdNotNull(parent);
        List<Integer> matchIds = new ArrayList<Integer>();
        Match m;
        for(Notification n : nList){
            m = matchRepository.findMatchWithMatchId(n.getMatchId());
            if(m.getEndTime().after(now)){
                matchIds.add(n.getMatchId());
            }
        }
        if(matchIds.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Inga aktiva matchningar");
        }
        List<MatchInfo> mi = new ArrayList<>();
        for(Integer i : matchIds){
            mi.add(getMatch(i));
        }

        return ResponseEntity.status(HttpStatus.OK).body(mi);
    }

    private String booleanConverterForMatch(boolean current, boolean matched){
        if (current && matched){return "both";}
        else if (current){return "you";}
        else{return "them";}
    }
}