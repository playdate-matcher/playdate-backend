package com.playdate.playdate.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FcmToken {
    private String fcmToken;
}
