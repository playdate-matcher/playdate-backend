package com.playdate.playdate.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Parents")
@Getter
@Setter
public class Parent {

    @Id
    private String email;

    private String firstName;
    private String surname;
    private String phoneNbr;
    private String password;
    private String address;
    private String fcmToken;

    /**
     * Creates a Parent with no parameters. WARNING: Is used although says it is not.
     */
    public Parent() {}
}