package com.playdate.playdate.models;

//Output for authentication method

public class AuthenticationResponse {

    private final String jwt;

    /**
     * Gives back a JWT token when the person has been authenticated
     * @param jwt the token that lets you into the app after you have verified yourself
     */
    public AuthenticationResponse(String jwt) {
        this.jwt = jwt;
    }

    /**
     * Gives back a JWT token
     * @return a JWT token
     */
    public String getJwt() {
        return jwt;
    }
}
