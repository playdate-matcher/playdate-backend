package com.playdate.playdate.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "FriendsWith")
@IdClass(FriendsWithId.class)
public class FriendsWith {

    @Id
    private String childFirstName1;
    @Id
    private String childParentEmail1;
    @Id
    private String childFirstName2;
    @Id
    private String childParentEmail2;
    private boolean frozen;

    /**
     * Creates a FriendsWith object with no parameters.
     */
    public FriendsWith(){}

    /**
     * Creates a FriendsWith object with the given parameters.
     * @param childFirstName1
     * @param childParentEmail1
     * @param childFirstName2
     * @param childParentEmail2
     */
    public FriendsWith(String childFirstName1, String childParentEmail1, String childFirstName2, String childParentEmail2){
        this.childFirstName1 = childFirstName1;
        this.childFirstName2 = childFirstName2;
        this.childParentEmail1 = childParentEmail1;
        this.childParentEmail2 = childParentEmail2;
        frozen = false;
    }

    /**
     * Compares an object to this FriendsWith.
     * @param o the object to be compared to.
     * @return true if both objects parent's emails and children's first names are the same.
     */
    @Override
    public boolean equals(Object o){
        FriendsWithId friendsWithId = new FriendsWithId(childFirstName1, childParentEmail1, childFirstName2, childParentEmail2);
        FriendsWith friendsWith = (FriendsWith) o;
        FriendsWithId friendsWithIdToBeCompared = new FriendsWithId(friendsWith.getChildFirstName1(), friendsWith.getChildParentEmail1(),
                                                friendsWith.getChildFirstName2(), friendsWith.getChildParentEmail2());
        return friendsWithId.equals(friendsWithIdToBeCompared);
    }
}