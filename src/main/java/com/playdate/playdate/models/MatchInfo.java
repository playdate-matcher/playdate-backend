package com.playdate.playdate.models;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
@Getter
@Setter
public class MatchInfo {

    private int matchId;
    private Timestamp startTime;
    private Timestamp endTime;
    private String myChildFullName;
    private String matchedChildFullName;
    private String matchedChildAllergies;
    private String matchedParentFullName;
    private String matchedParentAddress;
    private String matchedParentNumber;
    private String canGet;
    private String canDrop;
    private String canHost;

    /**
     * Creates a MatchInfo with no parameters.
     */
    public MatchInfo(){}
}