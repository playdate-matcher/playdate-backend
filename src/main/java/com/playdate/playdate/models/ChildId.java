package com.playdate.playdate.models;

import java.io.Serializable;

public class ChildId implements Serializable {
    private String firstName;
    private String parentEmail;

    /**
     * Creates a ChildId object with no parameters. WARNING: Is used by Spring Boot although says it is not used
     */
    public ChildId() {
    }

    /**
     * Creates a ChildId object with given parameters.
     * @param firstName the firstName of the child
     * @param parentEmail the parents email.
     */
    public ChildId(String firstName, String parentEmail) {
        this.firstName = firstName;
        this.parentEmail = parentEmail;
    }

    /**
     * Compares an object to this ChildId.
     * @param o the object to be compared to.
     * @return true if both objects parent's emails and children's first name are the same.
     */
    @Override
    public boolean equals(Object o){
        if(o instanceof ChildId){
            ChildId c = (ChildId) o;
            if(firstName.equals(c.firstName) && parentEmail.equals(c.parentEmail))
                return true;
        }
        return false;
    }
}