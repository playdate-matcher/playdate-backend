package com.playdate.playdate.models;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


@Getter
@Setter
public class NotificationHandler {

    private int id;
    private Timestamp created;
    private String parentEmail;
    private PlayDate playDate;
    private List<PlayDate> matches;

    /**
     * Creates a Notification with no parameters. WARNING: Is used although says it is not.
     */
    public NotificationHandler() {}

    /**
     * Creates a NotificationHandler with the given parameters.
     * @param id
     * @param created
     * @param parentEmail
     * @param playDate
     * @param matches
     */
    public NotificationHandler(int id, Timestamp created, String parentEmail, PlayDate playDate, List<PlayDate> matches) {
        this.id = id;
        this.created = created;
        this.parentEmail = parentEmail;
        this.playDate = playDate;
        this.matches = matches;
    }
}