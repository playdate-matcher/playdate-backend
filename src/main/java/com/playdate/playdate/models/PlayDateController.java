package com.playdate.playdate.models;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class PlayDateController {


    @Autowired
    PlayDateRepository playDateRepository;
    @Autowired
    NotificationRepository notificationRepository;

    /**
     * Saves the given PlayDate to the database.
     * @param playdate Fields to be completed are startTime, endTime, canGet, canDrop,
     *                 canHost and childFirstName. Example:
     *                       {
     * 	                        "startTime": "2020-01-01T12:01:01.000+0000",
     * 		                    "endTime": "2020-01-01T13:01:01.000+0000",
     * 		                    "canGet": "true",
     * 		                    "canDrop": "true",
     * 		                    "canHost": "true",
     * 		                    "childFirstName": "Hans"
     *                       }
     * @return  ResponseEntity<Object> saying "Play date has passed", "Play date already up"
     *                                 or "Play date created".
     */
    @ResponseBody
    @PostMapping("/playdate")
    public ResponseEntity<Object> createPlaydate(@RequestBody PlayDate playdate) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String cName = playdate.getChildFirstName();
        if("".equals(cName) || cName == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Vänligen ange vilket barn lekstunden gäller");
        }
        List<PlayDate> all = playDateRepository.findByChildParentEmailAndChildFirstName(auth.getName(), cName);
        all.removeIf(p -> (p.getMatchId() != null));
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        if(playdate.getForesight() != null) {
            if (playdate.getForesight().before(ts) || playdate.getForesight().after(playdate.getEndTime())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Vald deadline för matchning är ogiltig");
            }
        }
        if(playdate.getStartTime().before(ts)){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Lekstund har passerat");
        }
        if(playdate.getEndTime().before(playdate.getStartTime())){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Sluttid kan inte vara före starttid");
        }
        for (PlayDate oldPlay : all) {
            if (!(playdate.getStartTime().after(oldPlay.getEndTime()) ||
                    playdate.getEndTime().before(oldPlay.getStartTime()))) {
                if(oldPlay.getForesight().after(new Timestamp(System.currentTimeMillis())))
                    return ResponseEntity.status(HttpStatus.CONFLICT).body("En lekstund med dessa tider finns redan uppe");
            }
        }
        playdate.setChildParentEmail(auth.getName());
        playDateRepository.save(playdate);
        return ResponseEntity.status(HttpStatus.CREATED).body("Lekstund har skapats");
    }


    /**
     * Removes the play date with the provided playDateId from the database
     * @param playDateId A RequestParam where "id" is the key and the playDateId (example 40) is the value
     * @return ResponseEntity<Object> saying "Play date removed"
     */
    @ResponseBody
    @DeleteMapping("/playdate")
    public ResponseEntity<Object> removePlayDate(@RequestParam int playDateId){
        Notification n = notificationRepository.findByPlayDateId(playDateId);
        if(n != null){
            n.setPlayDateId(null);
            notificationRepository.save(n);
        }
        playDateRepository.delete(playDateRepository.getPlayDateId(playDateId));
        return ResponseEntity.status(HttpStatus.OK).body("Playdate borttagen");
    }

    /**
     * Retrieves all playDates in the future and the matchId is null.
     * @param firstName RequestParam where "firstName" is the key and the firstName (example Anna) is the value
     * @return
     */
    @ResponseBody
    @GetMapping("/playdate")
    public List<PlayDate> getFuturePlayDates(@RequestParam String firstName){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<PlayDate> all = playDateRepository.findByChildParentEmailAndChildFirstName(auth.getName(), firstName);
        List<PlayDate> futurePlayDates = new ArrayList<>();
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        for (PlayDate playDate : all) {
            boolean foresightOK = true;
            if(playDate.getForesight() != null){
                foresightOK = playDate.getForesight().after(ts);
            }
            if (ts.before(playDate.getEndTime()) && playDate.getMatchId() == null && foresightOK) {
                futurePlayDates.add(playDate);
            }
        }
        return futurePlayDates;
    }
}