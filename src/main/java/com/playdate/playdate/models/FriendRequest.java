package com.playdate.playdate.models;

import lombok.Getter;

@Getter
public class FriendRequest {

    /**
     * friendCode is the friendCode of the child to be added
     */
    private String firstName;
    /**
     * firstName is the first name of the child sending the FriendRequest
    */
     private String friendCode;

    /**
     * Creates a FriendRequest with no parameters
     */
    public FriendRequest(){
    }
}