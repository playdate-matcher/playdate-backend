package com.playdate.playdate.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.ArrayList;

@Entity
@Table(name = "Notifications")
@Getter
@Setter
public class Notification{

    @Id
    private int id;
    private Timestamp created;
    private String parentEmail;
    private Integer playDateId;
    private Integer matchId;

    /**
     * Creates a Notification with no parameters. WARNING: Is used although says it is not.
     */
    public Notification(){}
}