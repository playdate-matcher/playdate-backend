package com.playdate.playdate.models;

import lombok.Getter;
import lombok.Setter;

//Input argument for the authenticate method

/**
 * This class takes in the username and password that we put into our log in page as a request
 */
@Getter
@Setter
public class AuthenticationRequest {

    private String username;
    private String password;

    /**
     * Is used by Spring Boot
     */
    public AuthenticationRequest() {
    }

    /**
     *  Is used by Spring Boot
     * @param username the username you log in with
     * @param password the password you log in with
     */
    public AuthenticationRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
