package com.playdate.playdate.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FreezeFriend {
    private String firstName;
    private String friendsParentEmail;
    private String friendsName;
    private boolean freeze;

    /**
     * Creates an FreezeFriend object which is used to see if a used has frozen a friend or not
     * @param freeze if the friend is frozen or not
     * @param firstName the child who has frozen or not frozen its friend
     * @param friendParentEmail parent email to the friend
     * @param friendsName the friend who is frozen or not
     */
    FreezeFriend(boolean freeze, String firstName, String friendParentEmail, String friendsName, String parentsEmail ){
        this.freeze = freeze;
        this.firstName = firstName;
        this.friendsParentEmail = friendParentEmail;
        this.friendsName = friendsName;

    }
}