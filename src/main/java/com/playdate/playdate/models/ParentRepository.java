package com.playdate.playdate.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface ParentRepository extends JpaRepository<Parent, String> {

    /**
     * Deletes the parent with given email address.
     *
     * @param parentEmail
     */
    @Query(value = "delete FROM Parents where email = :parentEmail", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteParent(
            @Param("parentEmail") String parentEmail
    );

    @Query(value = "UPDATE Parents SET fcmToken = :fcmToken, surname = :surname, phoneNbr = :phoneNbr, address = :address WHERE email = :parentEmail", nativeQuery = true)
    @Modifying
    @Transactional
    void updateParent(
            @Param("parentEmail") String parentEmail,
            @Param("surname") String surname,
            @Param("phoneNbr") String phoneNbr,
            @Param("address") String address,
            @Param("fcmToken") String fcmToken

    );
    /**
     * Retrieves the parent with the given email
     *
     * @param parentEmail
     * @return Parent
     */
    @Query(value = "select * from Parents where email = :parentEmail", nativeQuery = true)
    Parent getParent(
            @Param("parentEmail") String parentEmail
    );

    /**
     * Retrieves the parentEmail to the given playDateId.
     * @param playDateId
     * @return String of parentEmail
     */
    @Query(value = "select childParentEmail from PlayDates where id  = :playDateId", nativeQuery = true)
    @Transactional
    String getParentEmail(
            @Param("playDateId") int playDateId
    );
}