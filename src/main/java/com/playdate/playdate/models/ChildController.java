package com.playdate.playdate.models;


import com.playdate.playdate.randomGenerator.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChildController {

    @Autowired
    ChildRepository childRepository;

    /**
     * Creates a child with the information given in the parameter.
     * @param child must contain firstName and if wanted can also contain surname and allergies. Example:
     *              {
     *                 "firstName": "Lisa",
     *                 "surname": "Andersson",
     *               }
     * @return a ResponseEntity saying if a child is either created or already exists.
     */
    @PostMapping("/child")
    public ResponseEntity<Object> createChild(@RequestBody Child child){
        List<Child> all = childRepository.findAll();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        for (Child oldChild : all) {
            if (oldChild.getParentEmail().equals(auth.getName()) &&
                    oldChild.getFirstName().equals(child.getFirstName())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Det finns redan ett barn med detta namnet");
            }
        }
        RandomString rand = new RandomString(7);
        String friendCode = rand.nextString();
        Child c = new Child(child.getFirstName(), child.getSurname(), auth.getName(), child.getAllergies(), friendCode);
        childRepository.save(c);
        return ResponseEntity.status(HttpStatus.OK).body("Ditt barn har skapats");
    }

    /**
     * To get a friend code of the given child.
     * @param firstName the first name of the child's friend code is requested. Example:
     *                       {
     *                           "firstName": "Lisa",
     *                       }
     * @return  Returns a string with a friend code if a child with that name is found and is the logged in users child. If not it returns
     *          an empty string.
     */
    @GetMapping("/friendcode")
    public String getFriendCode(@RequestParam String firstName){
        List<Child> code = childRepository.findByFirstNameContaining(firstName);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        for (Child child : code) {
            if (child.getParentEmail().equals(auth.getName()) && child.getFirstName().equals(firstName)) {
                return child.getFriendCode();
            }
        }
        return "";
    }
}