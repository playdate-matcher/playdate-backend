package com.playdate.playdate.models;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface MatchRepository extends JpaRepository<Match, String> {


    @Query(value = "DELETE from Matches WHERE id = :id", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteMatchWithMatchId(
            @Param("id") int matchId
    );

    /**
     * Retrieves the Match with the given MatchId.
     * @param matchId
     * @return Match
     */
    @Query(value = "select * from Matches where id = :matchId", nativeQuery = true)
        Match findMatchWithMatchId(
        @Param("matchId") int matchId
    );

    /**
     * Retrieves the Match with the given PlayDateId.
     * @param playDateId
     * @return Match
     */
    @Query(value = "select * from Matches where playDateID1 = :playDateId OR playDateID2 = :playDateId", nativeQuery = true)
    Match findMatchWithPlayDateId(
            @Param("playDateId") int playDateId
    );

    /**
     * Retrieves the Match with the given PlayDateIds.
     * @param p1 playDateId1
     * @param p2 playDateId2
     * @return Match object
     */
    Match findByPlayDateId1AndPlayDateId2(int p1, int p2);

    /**
     * Creates a Match with the information from the parameters.
     * @param startTime
     * @param endTime
     * @param playDateId1
     * @param playDateId2
     */
    @Query(value = "insert into Matches values(null, :startTime, :endTime, :playDateId1, :playDateId2)", nativeQuery = true)
    @Modifying
    @Transactional
    void createMatch(
            @Param("startTime") String startTime,
            @Param("endTime") String endTime,
            @Param("playDateId1") int playDateId1,
            @Param("playDateId2") int playDateId2);
}