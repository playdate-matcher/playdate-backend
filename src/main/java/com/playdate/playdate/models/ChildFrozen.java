package com.playdate.playdate.models;

import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class ChildFrozen {
    private Child child;
    private Boolean frozen;


    /**
     * Creates a ChildFrozen object.
     */
    public ChildFrozen(){
    }
}
