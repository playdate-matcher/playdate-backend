package com.playdate.playdate.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Table(name = "Matches")
@Getter
@Setter
public class Match {

    @Id
    private int id;
    private Timestamp startTime;
    private Timestamp endTime;
    private int playDateId1;
    private int playDateId2;
    /**
     * Creates a Match with no parameters. WARNING: Is used although says it is not.
     */
    public Match(){}

    /**
     *
     * @param id
     * @param startTime
     * @param endTime
     * @param playDateId1 one of the playDates connected to this match
     * @param playDateId2 the other playDate connected to this match
     */
    public Match(int id, Timestamp startTime, Timestamp endTime, int playDateId1, int playDateId2){
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.playDateId1 = playDateId1;
        this.playDateId2 = playDateId2;
    }
}