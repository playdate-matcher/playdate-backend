package com.playdate.playdate.models;

import java.io.Serializable;

public class FriendsWithId implements Serializable {

    private String childFirstName1;
    private String childParentEmail1;
    private String childFirstName2;
    private String childParentEmail2;

    /**
     * Creates a FriendsWithId with no parameters. WARNING: Is used although says it is not.
     */
    public FriendsWithId(){}

    /**
     * @param childFirstName1 first child's first name
     * @param childParentEmail1 first child's parent email
     * @param childFirstName2 second child's first child
     * @param childParentEmail2 second child's parent email
     */
    public FriendsWithId(String childFirstName1, String childParentEmail1, String childFirstName2, String childParentEmail2){
        this.childFirstName1 = childFirstName1;
        this.childFirstName2 = childFirstName2;
        this.childParentEmail1 = childParentEmail1;
        this.childParentEmail2 = childParentEmail2;
    }

    /**
     * @param o the object to be compared to.
     * @return if all attributes in both object are the same it returns true.
     */
    @Override
    public boolean equals(Object o){
        FriendsWithId fwi = (FriendsWithId) o;
        if(childFirstName1.equals(fwi.childFirstName1) && childParentEmail1.equals(fwi.childParentEmail1)){
            if(childFirstName2.equals(fwi.childFirstName2) && childParentEmail2.equals(fwi.childParentEmail2)){
                return true;
            }
        }
        return false;
    }
}