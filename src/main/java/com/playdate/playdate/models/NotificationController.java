package com.playdate.playdate.models;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class NotificationController {

    @Autowired
    NotificationRepository notificationRepository;

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    PlayDateRepository  playDateRepository;

    /**
     * Retrieves all notifications belonging to the current user.
     * @return list of NotificationHandler objects.
     */
    @GetMapping("/notification")
    public List<NotificationHandler> getNotifications(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Notification> id = notificationRepository.findByParentEmail(auth.getName());
        NotificationHandler notificationHandler = new NotificationHandler();
        List<NotificationHandler> notificationHandlerList = new ArrayList<NotificationHandler>();

        for(Notification notification : id) {
            if (notification.getMatchId() != null) {
                    NotificationHandler handler = new NotificationHandler(notification.getId(), notification.getCreated(), auth.getName(),
                            null,playDateRepository.getPlayDatesWithMatchId(notification.getMatchId()));
                    notificationHandlerList.add(handler);
            } else {
               NotificationHandler handler2 = new NotificationHandler(notification.getId(), notification.getCreated(), auth.getName(),
                       playDateRepository.getPlayDateId(notification.getPlayDateId()),null);
               notificationHandlerList.add(handler2);
            }
        }
        return notificationHandlerList;
    }
}