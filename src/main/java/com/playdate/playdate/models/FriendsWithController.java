package com.playdate.playdate.models;


import com.playdate.playdate.randomGenerator.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FriendsWithController {

    @Autowired
    FriendsWithRepository friendsWithRepository;

    @Autowired
    ChildRepository childRepository;

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    PlayDateRepository playDateRepository;

    private RandomString rand;

    /**
     * Creates a FriendsWith with the FriendRequest
     * @param friendRequest the firstName of the current users child and the friend code of the child to be added. Example:
     *                    {
     *                       "firstName": "Lisa",
     *                       "friendCode": "H89T6u"
     *                    }
     * @return a ResponseEntity saying if a FriendsWith is either created or already exists.
     */
    @ResponseBody
    @PostMapping("/friendswith")
    public ResponseEntity<Object> createFriendsWith(@RequestBody FriendRequest friendRequest){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String childName = friendRequest.getFirstName();
        String parentEmail = auth.getName();
        List<FriendsWith> all = friendsWithRepository.findByChildFirstName1AndChildParentEmail1(childName, parentEmail);
        Child friend = childRepository.findByFriendCode(friendRequest.getFriendCode());
        if (friendRequest.getFirstName() == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Inget barn är valt");
        }
        if(friend == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ogiltig vänkod");
        }
        String friendName = friend.getFirstName();
        String friendParent = friend.getParentEmail();
        FriendsWith newFriend = new FriendsWith(childName, parentEmail, friendName, friendParent);

        for(FriendsWith fw: all){
            if(fw.equals(newFriend)){
                return ResponseEntity.status(HttpStatus.CONFLICT).body("Vänskap finns redan");
            }
        }

        //uppdaterar friendCode för tillagd vän
        rand = new RandomString(7);
        String friendCode = rand.nextString();
        friend.setFriendCode(friendCode);
        childRepository.save(friend);
        //skapar friendsWith-rader åt båda hållen
        FriendsWith coupling = new FriendsWith(friendName, friendParent, childName, parentEmail);
        friendsWithRepository.save(newFriend);
        friendsWithRepository.save(coupling);

        return ResponseEntity.status(HttpStatus.CREATED).body("Du har nu lagt till " + friend.getFirstName() + " i din vänlista!");
    }

    /**
     * Retrieves a list of all the friends to the current users child with the parameter firstName
     * @param firstName firstName of the child. Example:
     *                        {
     *                            "firstName": "Lisa",
     *                        }
     * @return a list of Child objects
     */
    @GetMapping("/friendswith")
    public List<ChildFrozen> getFriends(@RequestParam String firstName) {

        List<FriendsWith> all = friendsWithRepository.findAll();
        List<Child> child = childRepository.findAll();
        List<ChildFrozen> friendsList = new ArrayList<>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        for (Child check: child ) {
            for (FriendsWith friendsWith : all) {
                if (auth.getName().equals(friendsWith.getChildParentEmail1()) && firstName.equals(friendsWith.getChildFirstName1()) &&
                        childRepository.findChild(friendsWith.getChildFirstName2(), friendsWith.getChildParentEmail2()).equals(check)) {
                    ChildFrozen frozen = new ChildFrozen();
                    frozen.setChild(check);
                    frozen.setFrozen(friendsWith.isFrozen());
                    friendsList.add(frozen);
                }
            }
        }
        return friendsList;
    }

    /**
     * Deletes the friendsWith between the two children in the deleteRequest.
     * @param deleteRequest with the info to be deleted. Example:
     *                {
     *                    "firstName": "Hans",
     *                    "friendsParentEmail": "karl@hotmail.com",
     *                    "friendsName": "Lilja"
     *                }
     * @return ResponseEntity<Object> saying friendship deleted
     */

    @DeleteMapping("/friendswith")
    public ResponseEntity<Object> deleteFriendsWith(@RequestBody DeleteRequest deleteRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        FriendsWith toBeDeleted = friendsWithRepository.findByChildFirstName1AndChildParentEmail1AndChildFirstName2AndChildParentEmail2(
                deleteRequest.getFirstName(), auth.getName(), deleteRequest.getFriendsName(), deleteRequest.getFriendsParentEmail());
        friendsWithRepository.delete(toBeDeleted);
        return ResponseEntity.status(HttpStatus.OK).body("Vänskap borttagen");
    }

    /**
     * Freezes(pauses) or unfreezes the current users child with another given child in the freezeFriend parameter. Freezing someone
     * means that you wont be matched with this child until unfrozen them.
     * @param freezeFriend the two children to be frozen. Example:
     *                     {
     *                	        "firstName": "Hans",
     *                	        "friendsName": "Anna",
     *                	        "friendParentEmail": "karin@hotmail.com",
     *       	                 "freeze": "true"
     *                      }
     * @return ResponseEntity<Object> saying friendship frozen or unfrozen
     */
    @PatchMapping("/freezefriend")
    public ResponseEntity<Object> freezeFriendsWith(@RequestBody FreezeFriend freezeFriend) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String parentEmail = auth.getName();
        String child = freezeFriend.getFirstName();
        List<FriendsWith> friends = friendsWithRepository.findByChildFirstName1AndChildParentEmail1(child, parentEmail);
        if(freezeFriend.isFreeze()) {
            int countFrozen = 0;
            for (FriendsWith fw : friends) {
                if (fw.isFrozen()) {
                    countFrozen++;
                }
            }
            if (countFrozen >= 2) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Du har redan pausat två vänskaper");
            }
        }
        friendsWithRepository.freezeFriend(freezeFriend.isFreeze(), child, freezeFriend.getFriendsName(),
                parentEmail, freezeFriend.getFriendsParentEmail());
        return (freezeFriend.isFreeze() ? ResponseEntity.status(HttpStatus.OK).body("Vänskap pausad")
                : ResponseEntity.status(HttpStatus.OK).body("Vänskap aktiverad"));
    }
}