package com.playdate.playdate.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ChildRepository extends JpaRepository<Child, String> {


    /**
     * Retrieves the Child with firstName and parentEmail found in the database
     *
     * @param   firstName first name of the child
     * @param   parentEmail parent email to the child parent
     * @return  the Child with the firstName and parentEmail found in the database
     */
    @Query(value = "select firstName, surname, parentEmail, allergies, friendCode from Children where firstName = :firstName AND parentEmail = :parentEmail", nativeQuery = true)
        Child findChild(
            @Param("firstName") String firstName,
            @Param("parentEmail") String parentEmail);


    /**
     * Retrieves all children with the parent email provided.
     * @param parentEmail
     * @return list of Child objects
     */
    List<Child> findByParentEmailContaining(String parentEmail);

    /**
     * Retrieves all children with the first name given
     * @param firstName the childrens first name
     * @return a list of Child objects
     */
    List<Child> findByFirstNameContaining(String firstName);

    /**
     * Finds the child with the given friendCode.
     * @param friendCode
     * @return Child
     */
    Child findByFriendCode(String friendCode);

    /**
     * Deletes all children from the database with the parent email given.
     * @param parentEmail
     */
    @Query(value = "DELETE FROM Children where parentEmail = :parentEmail", nativeQuery = true)
    @Modifying
    @Transactional
    void deleteParentsChildren(
            @Param("parentEmail") String parentEmail);
}