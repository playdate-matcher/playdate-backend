package com.playdate.playdate.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Children")
@IdClass(ChildId.class)
public class Child {

    @Id
    private String firstName;
    @Id
    private String parentEmail;
    private String friendCode;
    private String surname;
    private String allergies;

    /**
     * Creates a Child with no parameters. WARNING: Is used by Spring Boot although says it is not used.
     */
    public Child() { }

    /**
     * @param firstName the child's first name
     * @param surname the child's surname
     * @param parentEmail the child's parents email address
     * @param allergies the child's allergies
     * @param friendCode a code which is used to add friends.
     */
    public Child(String firstName, String surname, String parentEmail, String allergies, String friendCode) {
        this.firstName = firstName;
        this.surname = surname;
        this.allergies = allergies;
        this.parentEmail = parentEmail;
        this.friendCode = friendCode;
    }
}